// COULEURS CONSOLE gras
const colors = require('./js/colors').bold;

exports.assert = (step, condition) => {
  let testUnit = condition ? this.success(step) : this.error(step);
  // si un test est faux, le serveur est arrété
  if (!testUnit) {
    this.info('Echec des tests unitaires...');
    this.info('Fermeture du serveur.');
    process.exit(1);
  }
};

exports.success = msg => {
  console.log(`[${colors.green('OK')}] ${msg}`);
  return true;
};
exports.error = msg => {
  console.log(`[${colors.red('KO')}] ${msg}`);
  return false;
};

exports.info = msg => {
  console.log(`[${colors.yellow('INFO')}] ${msg}`);
};
