const TDD = require('./TDD');
// MESSAGES CONSOLE
const message = require('./js/messages');
/**
 * MONGODB
 */
const DB_USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_NAME = process.env.DB_NAME;
const DB_URI = `mongodb+srv://${DB_USER}:${DB_PASSWORD}@nws-gustocoffee-ojzxj.mongodb.net/${DB_NAME}?w=majority`;

const mongoose = require('mongoose');
const DB_OPTIONS = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  ssl: true,
  authSource: 'admin',
  retryWrites: true,
};

// db connexion
mongoose
  .connect(DB_URI, DB_OPTIONS)
  .then(() => {
    TDD.assert('MongoDB connexion', true);
  })
  .catch(error => {
    message.error('database/connect : ' + error);
    TDD.assert('MongoDB connexion', false);
  });

module.exports = mongoose;
