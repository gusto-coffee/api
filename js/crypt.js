const bcrypt = require('bcrypt');
const saltRounds = 10;

const CRYPT = {
  /**
   *
   * @param {String} text
   * @returns {String} texte encodé
   */
  crypt: text => bcrypt.hash(text, saltRounds),

  /**
   *
   * @param {String} text
   * @param {String} Hash
   * @returns {Boolean} true si le texte (text) correspond au texte encodé (hash)
   */
  decrypt: (text, Hash) => bcrypt.compare(text, Hash),
};

module.exports = CRYPT;
