const mongoose = require('mongoose');
const types = mongoose.Types;
const moment = require('moment');
// Schéma
const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'nom du produit manquant'],
    },
    description: {
      type: String,
      default: '',
      maxlength: 250,
    },
    points: {
      type: Number,
      default: 0,
    },
    offer: {
      type: Number,
      default: 100,
    },
    price: {
      type: Number,
      required: [true, 'prix manquant'],
    },
    currency: {
      type: String,
      default: 'euro',
    },
    option_id: [
      {
        type: types.ObjectId,
        ref: 'options',
        default: null,
      },
    ],
    category_id: [
      {
        type: types.ObjectId,
        ref: 'categories',
        default: null,
      },
    ],
    imageName: {
      type: String,
    },
  },
  { timestamps: true },
);

// Modèle
module.exports = mongoose.model('products', productSchema);
