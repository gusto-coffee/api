const mongoose = require('mongoose');
const types = mongoose.Types;
const moment = require('moment');
// Schéma
const optionSchema = new mongoose.Schema(
  {
    // exemple : supplément chocolat
    name: {
      type: String,
      required: [true, 'nom manquant'],
    },
    price: { type: Number, required: [true, 'prix manquant'] },
    currency: { type: String, default: 'euro' },
  },
  { timestamps: true },
);

// Modèle
module.exports = mongoose.model('options', optionSchema);
