const mongoose = require('mongoose');
const moment = require('moment');

// Schéma
const categorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'catégorie manquante'],
    },
  },
  { timestamps: true },
);

// Modèle
module.exports = mongoose.model('categories', categorySchema);
