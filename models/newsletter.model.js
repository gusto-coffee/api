const mongoose = require('mongoose');

// Schéma
const NewsLetterSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      match: [/\S+@\S+\.\S+/, 'courriel invalide'],
    },
  },
  { timestamps: true },
);

// Modèle
module.exports = mongoose.model('newsletters', NewsLetterSchema);
