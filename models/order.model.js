const mongoose = require('mongoose');
const moment = require('moment');
const types = mongoose.Types;

// Schéma
const orderSchema = new mongoose.Schema(
  {
    id_order: String, // numéro de commande simple correspondant à "user_reference" de shopping_basket.model et "req.headers.session"
    shopping_basket_id: {
      type: types.ObjectId,
      ref: 'shopping_baskets',
      default: null,
    },
    user_id: {
      type: types.ObjectId,
      ref: 'users',
      default: null,
    },
    price: Number,
    fidelity_points: { type: Number, default: 0 },
    currency: { type: String, default: 'euro' },
    // 0 = commande passée
    // 1 = commande payée
    // 2 = commande préparée
    // 3 = commande livrée
    status: { type: Number, default: 0 },
    payment_method: {
      type: String,
      default: '',
    },
  },
  { timestamps: true },
);

// Modèle
module.exports = mongoose.model('orders', orderSchema);
