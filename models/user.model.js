const mongoose = require('mongoose');
const moment = require('moment');

// Schéma
const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      match: [/\S+@\S+\.\S+/, 'courriel invalide'],
      required: [true, 'courriel manquant'],
    },
    password: {
      type: String,
      minlength: 10,
      required: [true, 'mot de passe manquant'],
    },

    fidelity_points: { type: Number, default: 0 },

    lastname: {
      type: String,
      required: [true, 'nom manquant'],
    },
    firstname: {
      type: String,
      required: [true, 'prénom manquant'],
    },
    role: {
      type: String,
      default: 'client',
    }, //client, employee, admin
    avatar: {
      type: String,
      default: 'defaut.png',
    },
    updated_by: {
      type: String,
      default: '',
    },
  },
  {
    timestamps: true,
  },
);

// Modèle
module.exports = mongoose.model('users', userSchema);
