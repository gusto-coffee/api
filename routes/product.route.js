const express = require('express');
const router = express.Router();

// Validation des champs
const { check, validationResult } = require('express-validator');
// controller
const productController = require('../controllers/product.controller');

// MESSAGES CONSOLE
const message = require('../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour

/**
 * GET
 */
router.get('/', productController.getProducts);

router.get('/promotion', productController.getAllPromotions);

router.get('/category/:category_id', productController.getProductsCategory);

router.get('/:id', productController.getProduct);

module.exports = router;
