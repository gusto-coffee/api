const express = require('express');
const router = express.Router();

// Validation des champs
const { check, validationResult } = require('express-validator');
// controller
const coworkingController = require('../../controllers/admin/coworking.admin.controller');

// MESSAGES CONSOLE
const message = require('../../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour

/**
 * POST
 */
// nouvelle catégorie d'emplacement
router.post('/new', coworkingController.newLocationCategory);
// nouvel emplacement
router.post('/location/new', coworkingController.newLocation);
/**
 * PATCH
 */
// modifier catégorie d'emplacement
router.patch('/:id', coworkingController.editLocationCategory);
// modifier emplacement
router.patch('/location/:id', coworkingController.editLocation);
/**
 * DELETE
 */
// supprimer catégorie d'emplacement
router.delete('/:id', coworkingController.deleteLocationCategory);
// supprimer emplacement
router.delete('/location/:id', coworkingController.deleteLocation);

module.exports = router;
