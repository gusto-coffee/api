const express = require('express');
const router = express.Router();

// Validation des champs
const { check, validationResult } = require('express-validator');
// controller
const productAdminController = require('../../controllers/admin/product.admin.controller');

// MESSAGES CONSOLE
const message = require('../../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour

/**
 * POST
 */
router.post('/new', productAdminController.newProduct);

/**
 * PATCH
 */
router.patch('/:id', productAdminController.editProduct);

/**
 * DELETE
 */
router.delete('/:id', productAdminController.deleteProduct);

module.exports = router;
