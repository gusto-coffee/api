const express = require('express');
const router = express.Router();

// controller
const optionController = require('../../controllers/admin/option.admin.controller');

// MESSAGES CONSOLE
const message = require('../../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour

/**
 * POST
 */
router.post('/new', optionController.newOption);

/**
 * PATCH
 */
router.patch('/:id', optionController.editOption);

/**
 * DELETE
 */
router.delete('/:id', optionController.deleteOption);

module.exports = router;
