const express = require('express');
const router = express.Router();

// controller
const shoppingBasketController = require('../controllers/shopping_basket.controller');

// MESSAGES CONSOLE
const message = require('../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour

/**
 * GET
 */
// récupérer son panier
router.get('/:id', shoppingBasketController.getBasket);

// récupérer un panier avec le token utilisateur
router.get('/user-basket', shoppingBasketController.getBasketWithUserId);

/**
 * POST
 */
// ajouter un article dans son panier
router.post('/new', shoppingBasketController.addArticleIntoBasket);
// confirmer le panier pour passer à l'étape du paiement
router.post('/confirm', shoppingBasketController.confirmBasket);
/**
 * PATCH
 */
router.patch('/confirm/:id', shoppingBasketController.editConfirmBasket);
/**
 * DELETE
 */
// suppression d'un article du panier
router.delete('/:article_id', shoppingBasketController.deleteProductIntoBasket);

module.exports = router;
