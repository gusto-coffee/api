const express = require('express');
const router = express.Router();

// Validation des champs
const { check, validationResult } = require('express-validator');
// controller
const reservationController = require('../controllers/reservation.controller');

// MESSAGES CONSOLE
const message = require('../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour

/**
 * GET
 */
router.get('/:id', reservationController.getDayReservation);

/**
 * POST
 */
router.post('/new', reservationController.newHourReservation);

module.exports = router;
