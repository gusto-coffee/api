# Gusto Coffee API ☕

## Prérequis 🔧

- Node 10.x.x
- Docker (en réflexion)
- NPM / Yarn

## Installation 🔄

```
git clone
```

```
cd <projet>
```

Editer un fichier `.env` à la racine du projet.

- DB_USER
- DB_PASSWORD
- DB_NAME
- SECRET_KEY (clé pour le chiffrement)

```
npm install
```

## Lancement 🚀

```
node index.js
```

## Tests ✔

Affichage dans la console lors de la phase de lancement du serveur.

- [<span style="color:green">OK</span>] : test passé avec succès
- [<span style="color:red">KO</span>] : échec du test

Si un test échoue, un message [<span style="color:#f1c40f">INFO</span>] apparaît pour signaler la fermerture forcée de l'application.

<u>Tests effectués : </u>

- Lancement serveur avec le port d'écoute
- Connexion à la base de données MongoDB Atlas
