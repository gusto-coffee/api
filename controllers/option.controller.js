// models
const Option = require('../models/option.model');

// messages console
const message = require('../js/messages');

/**
 * Récupérer toutes les options
 * @returns {Response} 200 | 500
 */
exports.getOptions = (req, res) => {
  Option.find()
    .sort({ name: 1 })
    .then(options => {
      res.status(200).json(options);
    })
    .catch(error => {
      message.error('option.controller/getoptions : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer une option
 * @returns {Response} 200 | 404 | 500
 */
exports.getOption = (req, res) => {
  Option.findById(req.params.id)
    .then(option => {
      if (!option) res.status(404).json({ error: 'Option introuvable' });
      else res.status(200).json(option);
    })
    .catch(error => {
      message.error('option.controller/getOption : ' + error);
      res.status(500).json({ error });
    });
};
