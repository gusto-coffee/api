// models
const Product = require('../models/product.model');

const jwt = require('jsonwebtoken');
// messages console
const message = require('../js/messages');

/**
 * Récupérer tous les produits
 * @returns {Response} 200 | 500
 */
exports.getProducts = (req, res) => {
  Product.find()
    .sort({ name: 1 })
    .populate('category_id')
    .populate('option_id')
    .then(products => {
      res.status(200).json(products);
    })
    .catch(error => {
      message.error('product.controller/getProducts : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer un produit
 * @returns {Response} 200 | 404 | 500
 */
exports.getProduct = (req, res) => {
  Product.findById(req.params.id)
    .populate('category_id')
    .populate('option_id')
    .then(product => {
      if (!product) res.status(404).json({ error: 'Produit introuvable' });
      else res.status(200).json(product);
    })
    .catch(error => {
      message.error('product.controller/getProduct : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer tous les produits correspondants à une catégory
 * @returns {Response} 200 | 500
 */
exports.getProductsCategory = (req, res) => {
  Product.find({ category_id: req.params.category_id })
    .populate('category_id')
    .populate('option_id')
    .then(products => {
      res.status(200).json(products);
    })
    .catch(error => {
      message.error('product.controller/getProductsCategory : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer tous les offres concernants les produits triées par ordre croissant
 * @returns {Response} 200 | 500
 */
exports.getAllPromotions = (req, res) => {
  Product.find()
    .populate('category_id')
    .populate('option_id')
    .then(products => {
      res.status(200).json(products);
    })
    .catch(error => {
      message.error('product.controller/getAllPromotions : ' + error);
      res.status(500).json({ error });
    });
};
