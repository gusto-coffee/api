// models
const Product = require('../../models/product.model');

const jwt = require('jsonwebtoken');
// messages console
const message = require('../../js/messages');
const capitalizeFirstLetter = require('../../js/functions')
  .capitalizeFirstLetter;

/**
 * Ajouter un produit
 * @returns {Response} 201 | 400 | 401 | 500
 */
exports.newProduct = (req, res) => {
  Product.findOne({ name: req.body.name })
    .then(product => {
      // si existant
      if (product) throw { code: 400 };
      // si il n'existe pas
      else
        return new Product({
          name: capitalizeFirstLetter(req.body.name),
          price: req.body.price,
          option_id: req.body.option_id,
          category_id: req.body.category_id,
        }).save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      if (error.code === 400)
        res.status(400).json({ error: 'Produit déjà existant' });
      else {
        message.error('product.admin.controller/newProduct : ' + error);
        res.status(500).json({ error });
      }
    });
};

/**
 * Modifier un produit
 * @returns {Response} 201 | 401 | 404 | 500
 */
exports.editProduct = (req, res) => {
  Product.findById(req.params.id)
    .then(product => {
      if (!product) throw { code: 404 };
      else
        return product
          .set({
            name: capitalizeFirstLetter(req.body.name),
            price: req.body.price,
            option_id: req.body.option_id,
            category_id: req.body.category_id,
          })
          .save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      if (error.code === 404)
        res.status(404).json({ error: 'Produit introuvable' });
      else {
        message.error('product.admin.controller/editProduct : ' + error);
        res.status(500).json({ error });
      }
    });
};

/**
 * Supprimer un produit
 * @returns {Response} 200 | 401 | 404 | 500
 */
exports.deleteProduct = (req, res) => {
  Product.findById(req.params.id)
    .then(product => {
      if (!product) throw { code: 404 };
      else return product.remove();
    })
    .then(() => {
      res.sendStatus(200);
    })
    .catch(error => {
      if (error.code === 404)
        res.status(404).json({ error: 'Produit introuvable' });
      else {
        message.error('product.admin.controller/deleteProduct : ' + error);
        res.status(500).json({ error });
      }
    });
};
