// modèles
const LocationCategory = require('../../models/location_category.model');
const Location = require('../../models/location.model');

const jwt = require('jsonwebtoken');
// messages console
const message = require('../../js/messages');

/**
 * POST
 */

/**
 * Ajouter une nouvelle catégory d'emplacement
 * @returns {Response} 201 | 400 | 401 | 500
 */
exports.newLocationCategory = (req, res) => {
  LocationCategory.findOne({ name: req.body.name })
    .then(location_category => {
      if (location_category) throw { code: 400 };
      else return new LocationCategory(req.body).save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      // condition avec le throw au début afin de casser la chaîne des promesses
      if (error.code === 400)
        res.status(400).json({
          error: "Catégorie d'emplacement déjà existante",
        });
      else {
        message.error(
          'coworking.admin.controller/newLocationCategory : ' + error,
        );
        res.status(500).json({ error });
      }
    });
};

/**
 * Ajout d'un nouvel emplacement
 * @returns {Response} 201 | 400 | 401 | 500
 */
exports.newLocation = (req, res) => {
  Location.findOne({ name: req.body.name })
    .then(location => {
      if (location) throw { code: 400 };
      else return new Location(req.body).save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      // condition avec le throw au début afin de casser la chaîne des promesses
      if (error.code === 400)
        res.status(400).json({
          error: 'Emplacement déjà existant',
        });
      else {
        message.error('coworking.admin.controller/newLocation : ' + error);
        res.status(500).json({ error });
      }
    });
};

/**
 * PATCH
 */

/**
 * Modification d'une catégorie d'emplacement
 * @returns {Response} 201 | 401 | 404 | 500
 */
exports.editLocationCategory = (req, res) => {
  LocationCategory.findById(req.params.id)
    .then(location_category => {
      if (!location_category) throw { code: 404 };
      else return location_category.set(req.body).save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      // condition avec le throw au début afin de casser la chaîne des promesses
      if (error.code === 404)
        res.status(404).json({
          error: "Catégorie d'emplacement introuvable",
        });
      else {
        message.error(
          'coworking.admin.controller/editLocationCategory : ' + error,
        );
        res.status(500).json({ error });
      }
    });
};

/**
 * Modification d'un emplacement
 * @returns {Response} 200 | 401 | 404 | 500
 */
exports.editLocation = (req, res) => {
  Location.findById(req.params.id)
    .then(location => {
      if (!location) throw { code: 404 };
      else return location.set(req.body).save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      // condition avec le throw au début afin de casser la chaîne des promesses
      if (error.code === 404)
        res.status(404).json({
          error: 'Emplacement introuvable',
        });
      else {
        message.error('coworking.admin.controller/editLocation : ' + error);
        res.status(500).json({ error });
      }
    });
};

/**
 * DELETE
 */

/**
 * Suppression d'une catégorie d'emplacement
 * @returns {Response} 200 | 401 | 404 | 500
 */
exports.deleteLocationCategory = (req, res) => {
  LocationCategory.findById(req.params.id)
    .then(location_category => {
      if (!location_category) throw { code: 404 };
      else return location_category.remove();
    })
    .then(() => {
      res.sendStatus(200);
    })
    .catch(error => {
      // condition avec le throw au début afin de casser la chaîne des promesses
      if (error.code === 404)
        res.status(404).json({
          error: "Catégorie d'emplacement introuvable",
        });
      else {
        message.error(
          'coworking.admin.controller/deleteLocationCategory : ' + error,
        );
        res.status(500).json({ error });
      }
    });
};

/**
 * Suppression d'un emplacement
 * @returns {Response} 200 | 401 | 404 | 500
 */
exports.deleteLocation = (req, res) => {
  Location.findById(req.params.id)
    .then(location => {
      if (!location) throw { code: 404 };
      else return location.remove();
    })
    .then(() => {
      res.sendStatus(200);
    })
    .catch(error => {
      // condition avec le throw au début afin de casser la chaîne des promesses
      if (error.code === 404)
        res.status(404).json({
          error: 'Emplacement introuvable',
        });
      else {
        message.error('coworking.admin.controller/deleteLocation : ' + error);
        res.status(500).json({ error });
      }
    });
};
