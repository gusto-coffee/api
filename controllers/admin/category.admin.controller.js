// models
const Category = require('../../models/category.model');

// messages console
const message = require('../../js/messages');
const capitalizeFirstLetter = require('../../js/functions')
  .capitalizeFirstLetter;

/**
 * Ajouter une catégorie
 * @returns {Response} 201 | 400 | 401 | 500
 */
exports.newCategory = (req, res) => {
  Category.findOne({ name: req.body.name })
    .then(category => {
      // si elle existe
      if (category) throw { code: 400 };
      // si elle n'existe pas
      else
        return Category.create({
          name: capitalizeFirstLetter(req.body.name),
        });
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      if (error.code === 400)
        res.status(400).json({ error: 'Catégorie déjà existante' });
      else {
        message.error('category.admin.controller/newCategory : ' + error);
        res.status(500).json({ error });
      }
    });
};

/**
 * Modifier une catégorie
 * @returns {Response} 201 | 404 | 401 | 500
 */
exports.editCategory = (req, res) => {
  Category.findById(req.params.id)
    .then(category => {
      if (!category) throw { code: 404 };
      else
        return category
          .set({
            name: capitalizeFirstLetter(req.body.name),
          })
          .save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      if (error.code === 404)
        res.status(404).json({ error: 'Catégorie introuvable' });
      else {
        message.error('category.controller/editCategory : ' + error);
        res.status(500).json({ error });
      }
    });
};

/**
 * Supprimer une catégorie
 * @returns {Response} 200 | 404 | 401 | 500
 */
exports.deleteCategory = (req, res) => {
  Category.findById(req.params.id)
    .then(category => {
      if (!category) throw { code: 404 };
      else return category.remove();
    })
    .then(() => {
      res.sendStatus(200);
    })
    .catch(error => {
      if (error.code === 404)
        res.status(404).json({ error: 'Catégorie introuvable' });
      else {
        message.error('category.controller/deleteCategory : ' + error);
        res.status(500).json({ error });
      }
    });
};
