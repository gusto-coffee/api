const randomstring = require('randomstring');
const moment = require('moment');

// fonctions
const jwt = require('../js/token');
const {
  findBasket,
  findBasketWithUserId,
  findOrder,
} = require('../js/functions');

// modèles
const ShoppingBasket = require('../models/shopping_basket.model');
const Reservation = require('../models/reservation.model');
const Order = require('../models/order.model');
const Product = require('../models/product.model');
const Option = require('../models/option.model');

// messages console
const message = require('../js/messages');

/**
 * récupérer le panier en cours
 * @returns {Response} 200 | 404 | 500
 */
exports.getBasket = (req, res) => {
  // récupérer le panier de l'utilisateur
  findBasket(req.params.id)
    .then(basket => {
      if (!basket) res.status(404).json({ error: 'Panier introuvable' });
      else
        res.status(200).json({
          basket,
        });
    })
    .catch(error => {
      message.error('shopping_basket.controller/getBasket : ' + error);
      res.status(500).json({ error });
    });
};

exports.getBasketWithUserId = (req, res) => {
  const { userId } = jwt.getUserInfos(req, res);
  findBasketWithUserId(userId)
    .then(basket => {
      if (!basket) res.status(404).json({ error: 'Panier introuvable' });
      else res.status(200).json(basket);
    })
    .catch(error => {
      message.error(
        'shopping_basket.controller/getBasketWithUserId : ' + error,
      );
      res.status(500).json({ error });
    });
};

/**
 * ajouter un article dans le panier de l'utilisateur
 * @returns {Response} 201 | 400 | 500
 */
exports.addArticleIntoBasket = (req, res) => {
  let price = 0.0;
  findOrder(req.headers.session)
    .then(order => {
      // si la commande est confirmée
      if (order)
        if (order.is_paid)
          // si la commande est payée on ne peut pas ajouter dans le panier
          throw {
            code: 400,
          };
        // recherche du produit pour récupérer ses infos
        else return Product.findById(req.body.product_id);
      else return Product.findById(req.body.product_id);
    })
    .then(product => {
      // ajout du prix du produit en le multipliant par sa quantité
      price = product.price * req.body.quantity;
      // recherche de toutes les options
      return Option.find().where('_id').in(req.body.option_ids);
    })
    .then(options => {
      // ajout du prix de chaque option
      options.forEach(item => {
        price += item.price;
      });
      return findBasket(req.headers.session);
    })
    .then(basket => {
      let articles = [];
      let total_price = 0.0;
      // si le panier existe déjà
      if (basket) {
        // on récupère les anciens articles
        articles = basket.articles;
        // on additionne l'ancien prix total avec le prix du produit + options actuel
        total_price = basket.total_price + price;

        articles.push({
          product_id: req.body.product_id,
          option_ids: req.body.option_ids,
          quantity: req.body.quantity,
          price,
        });
        return basket
          .set({
            articles,
            total_price,
          })
          .save();
      }
      // s'il n'existe pas on créé un nouveau panier
      else
        return new ShoppingBasket({
          user_reference: req.headers.session,
          articles: [
            {
              product_id: req.body.product_id,
              option_ids: req.body.option_ids,
              price,
            },
          ],
          total_price: price,
        }).save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      // condition avec le throw au début afin de casser la chaîne des promesses
      if (error.code === 400)
        res.status(400).json({
          error: "Impossible d'ajouter un article dans une commande déjà payéé",
        });
      else {
        message.error(
          'shopping_basket.controller/addArticleIntoBasket : ' + error,
        );
        res.status(500).json({ error });
      }
    });
};

/**
 * confirmation du panier avant de passer à l'étape du paiement
 * cette étape implique la création d'un modèle dans order.model
 * le modèle shopping_basket est un modèle temporaire
 * @returns {Response} 200 | 404 | 500
 */
exports.confirmBasket = (req, res) => {
  let userId = null;
  if (req.headers.authorization) userId = jwt.getUserInfos(req, res).userId;

  new ShoppingBasket({
    articles: req.body.articles,
    reservations: req.body.reservations,
    total_price: req.body.total_price,
    preview_fidelity_points: req.body.preview_fidelity_points,
    user_id: userId,
  })
    .save()
    .then(basket => {
      res.status(201).json({ _id: basket._id });
    })
    .catch(error => {
      message.error('shopping_basket.controller/confirmBasket : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * confirmation du panier avant de passer à l'étape du paiement
 * cette étape implique la création d'un modèle dans order.model
 * le modèle shopping_basket est un modèle temporaire
 * @returns {Response} 200 | 404 | 500
 */
exports.editConfirmBasket = (req, res) => {
  let userId = null;
  if (req.headers.authorization) userId = jwt.getUserInfos(req, res).userId;
  ShoppingBasket.findById(req.params.id)
    .then(basket => {
      if (!basket) throw { code: 404 };
      else
        return basket
          .set({
            articles: req.body.articles,
            reservations: req.body.reservations,
            total_price: req.body.total_price,
            preview_fidelity_points: req.body.preview_fidelity_points,
            user_id: userId,
          })
          .save();
    })

    .then(basket => {
      res.status(201).json({ _id: basket._id });
    })
    .catch(error => {
      if (error.code === 404)
        res.status(404).json({ error: 'Panier introuvable' });
      else {
        message.error(
          'shopping_basket.controller/editConfirmBasket : ' + error,
        );
        res.status(500).json({ error });
      }
    });
};

/**
 * suppression d'un article dans le panier
 * @returns {Response} 200 | 400 | 404 | 500
 */
exports.deleteProductIntoBasket = (req, res) => {
  /* console.log(req.headers.session);
  // vérifier si la commande n'a pas été payée
  findOrder(req.headers.session)
    .then(order => {
      // si la commande est confirmée
      if (order)
        if (order.is_paid)
          // si la commande est payée on ne peut pas supprimer dans le panier
          throw { code: 400 };
        // sinon on fait une recherche sur son panier
        else
          return ShoppingBasket.findOne({
            user_reference: req.headers.session,
          });
      else
        return ShoppingBasket.findOne({ user_reference: req.headers.session });
    })
    .then(basket => {
      // si le panier est inexistant
      if (!basket) throw { code: 404 };
      else {
        let articles = basket.articles;
        let total_price = basket.total_price;

        // suppression de l'article
        for (let i = 0; i < articles.length; i++) {
          if (String(articles[i]._id) === String(req.params.article_id)) {
            total_price -= articles[i].price; // mise à jour du prix
            articles.splice(i, 1); // suppression du produit
          }
        }
        return basket
          .set({
            date_str: moment().locale('fr').format('LLLL'),
            date: Date.now(),
            total_price,
            articles,
          })
          .save();
      }
    })
    .then(() => {
      res.sendStatus(200);
    })
    .catch(error => {
      // condition avec le throw au début afin de casser la chaîne des promesses
      if (error.code === 400)
        res.status(400).json({
          error:
            "Impossible de supprimer un article d'une commande déjà payéé.",
        });
      else if (error.code === 404)
        res.status(404).json({ error: 'Panier introuvable' });
      else {
        message.error(
          'shopping_basket.controller/deleteProductIntoBasket : ' + error,
        );
        res.status(500).json({ error });
      }
    });*/
};
