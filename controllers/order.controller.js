const randomstring = require('randomstring');
const moment = require('moment');

// fonctions
const jwt = require('../js/token');
const {
  createRandomString,
  findBasket,
  findBasketWithUserId,
  findOrder,
  findUserOrders,
  updateFidelityPoints,
  sendMailWhenOrderIsPaid,
} = require('../js/functions');

// modèles
const Order = require('../models/order.model');
const ShoppingBasket = require('../models/shopping_basket.model');
const User = require('../models/user.model');

// messages console
const message = require('../js/messages');

/**
 * Client
 * récupération de la commande
 * @returns {Response} 200 | 404 | 500
 */
exports.getOrder = (req, res) => {
  findOrder(req.params.id)
    .then(order => {
      if (!order) res.status(404).json({ error: 'Commande introuvable' });
      else res.status(200).json(order);
    })
    .catch(error => {
      message.error('order.controller/getOrder : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Client
 * récupération des commandes de l'utilisateur
 * @returns {Response} 200 | 401 | 500
 */
exports.getUserOrders = (req, res) => {
  if (req.headers.authorization) {
    const { userId } = jwt.getUserInfos(req, res);
    findUserOrders(userId)
      .then(orders => {
        res.status(200).json(orders);
      })
      .catch(error => {
        message.error('order.controller/getUserOrders : ' + error);
        res.status(500).json({ error });
      });
  } else res.sendStatus(401);
};

/**
 * si le paiement a été confirmé (banque, paypal, etc)
 * on met à jour la variable status dans order.model
 * si l'utilisateur est connecté, ses points de fidélité seront mis à jour
 * @returns {Response} 201 | 404 | 500
 */
exports.payment = (req, res) => {
  let user = null;
  findBasket(req.body._id)
    .then(basket => {
      if (!basket) throw { code: 404 };
      // si l'utilisateur a commandé des produits, on vérifie si c'est un utilisateur connecté avec un compte
      // pour associer ses points fidélités à son compte
      else {
        // si l'utilisateur dispose d'un compte client alors il a un token dans le header de ses requêtes
        if (req.headers.authorization) {
          user = jwt.getUserInfos(req, res);
          updateFidelityPoints(user.userId, basket.preview_fidelity_points);
        }
        //TODO: faire la même avec les emplacements

        //TODO: vérifier le paiement
        //TODO: faire la même condition que order_id avec la choix des emplacements
        return new Order({
          status: 1, // mise à jour de la commande, 1 = payée
          currency: req.body.currency,
          payment_method: req.body.payment_method,
          shopping_basket_id: basket._id,
          price: basket.total_price,
          fidelity_points: basket.preview_fidelity_points,
          id_order: createRandomString(),
          user_id: user ? user.userId : null,
        }).save();
      }
    })
    .then(order => {
      // si l'utilisateur dispose d'un mail
      if (user !== null) sendMailWhenOrderIsPaid(order._id, user.email);
      res.status(201).json({
        _id: order._id,
        message: `Commande ${order.id_order} acceptée`,
      });
    })
    .catch(error => {
      if (error.code === 404)
        res.status(404).json({ errror: 'Commande introuvable' });
      else {
        message.error('order.controller/payment : ' + error);
        res.status(500).json({ error });
      }
    });
};
